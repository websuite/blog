import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';

import { MaterialComponentsModule } from './material-components.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { BlogPageComponent } from './pages/blog-page/blog-page.component';
import { PostingPageComponent } from './pages/posting-page/posting-page.component';
import { BlogCardComponent } from './elements/blog-card/blog-card.component';
import { BlogPostingComponent } from './elements/blog-posting/blog-posting.component';


@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    BlogPageComponent,
    PostingPageComponent,
    BlogCardComponent,
    BlogPostingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, ReactiveFormsModule,
    FlexLayoutModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),
    MaterialComponentsModule,
    AppRoutingModule
  ],
  providers: [
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
