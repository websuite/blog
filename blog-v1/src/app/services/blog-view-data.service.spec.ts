import { TestBed, inject } from '@angular/core/testing';

import { BlogViewDataService } from './blog-view-data.service';

describe('BlogViewDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BlogViewDataService]
    });
  });

  it('should be created', inject([BlogViewDataService], (service: BlogViewDataService) => {
    expect(service).toBeTruthy();
  }));
});
